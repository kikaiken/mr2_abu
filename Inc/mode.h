/*
 * mode.h
 *
 *  Created on: 2018/06/19
 *      Author: Ryohei
 */

#ifndef MODE_H_
#define MODE_H_


#include "stm32f4xx_hal.h"

//グローバル変数
extern volatile int8_t contactless_sig;
extern volatile int8_t uukhai_flag;

//プロトタイプ宣言
void Mode0(void);
void Mode1(void);
void Mode2(void);
void Mode3(void);
void Mode4(void);
void Mode5(void);
void Mode6(void);
void Mode7(void);
void Mode8(void);
void Mode9(void);
void ExecMode(int8_t);


#endif /* MODE_H_ */
