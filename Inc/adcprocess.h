/*
 * wall_sensor.h
 *
 *  Created on: 2019/03/03
 *      Author: Ryohei
 */

#ifndef ADCPROCESS_H_
#define ADCPROCESS_H_

#include "stm32f4xx_hal.h"
#include "adc.h"
#include "robotstate.h"

//ADCハンドラ
#define ADC_HANDLER		hadc1

//ゲルゲレールポテンショギア歯数
#define GERGE_POT_GEAR_DIAMETER		0.030f

//構造体
typedef struct {
	float front_left;
	float front_right;
	float left_front;
	float left_rear;
	float right_front;
	float right_rear;
	float rear_left;
	float rear_right;
} wall_sens_s;

typedef struct {
	float side_wall_error;
	float front_wall_dist;
	float front_wall_angle;
	float rear_wall_dist;
	float rear_wall_angle;
} wall_pos_s;

//プロトタイプ宣言
void GetPosFromWall(wall_pos_s *, wall_sens_s);
void WallSensorGetDist(wall_sens_s *);
void GetGergePos(void);
void ADCEnable(void);
void ADCDisable(void);

#endif /* ADCPROCESS_H_ */
