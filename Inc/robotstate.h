/*
 * robotstate.h
 *
 *  Created on: 2018/06/19
 *      Author: Ryohei
 */

#ifndef ROBOTSTATE_H_
#define ROBOTSTATE_H_


#include "stm32f4xx_hal.h"
#include "lsm6ds3.h"
#include "tim.h"

typedef struct {
	volatile float x;
	volatile float y;
	volatile float theta;
} pos_s;

typedef struct {
	volatile float roll;
	volatile float pitch;
	volatile float yaw;
} angle_s;

typedef enum {
	MM_START_TO_SANDDUNE,
	MM_START_TO_SANDDUNE_PREPARE,

	MM_SANDDUNE,
	MM_SANDDUNE_PREPARE,

	MM_SANDDUNE_TO_TUSSOCK,
	MM_SANDDUNE_TO_TUSSOCK_PREPARE,

	MM_TUSSOCK,
	MM_TUSSOCK_PREPARE,

	MM_MOUNTAIN,
	MM_MOUNTAIN_PREPARE,

	MM_STOP,
} move_mode_e;

typedef enum {
	FIELD_RED,
	FIELD_BLUE
} field_select_e;

typedef enum {
	GERGE_MOVE_READY,
	GERGE_MOVE_START,
	GERGE_MOVE_STOP
} gerge_move_e;

typedef struct {
	volatile move_mode_e move_mode;

	//機体姿勢
	volatile float vel;						//速さ
	volatile float angvel;					//角速度
	volatile float angvel_error_integ;
	volatile angle_s angle;

	//機体位置
	volatile pos_s odom;

	//ゲルゲ機構
	volatile float gerge_pos;					//ゲルゲ機構位置 : offsetを差し引いて、一番下をゼロとした値
	volatile float prev_gerge_pos;				//ゲルゲ機構前の位置 : D制御用
	volatile float gerge_pos_offset;			//ゲルゲ機構一番下での絶対的な値
	volatile float gerge_pos_temp;				//ゲルゲ機構位置一時保存用
	volatile gerge_move_e gerge_move_flag;		//台形加減速実行/停止フラグ
} state_s;

//グローバル変数
extern state_s state;
extern pos_s integ_pos;

//プロトタイプ宣言
void GetOdometry(imu_t);
void SetMoveMode(move_mode_e);
void MR2CtrlStart(void);
void MR2CtrlStop(void);

#endif /* ROBOTSTATE_H_ */
