/*
 * gerge.h
 *
 *  Created on: 2018/12/23
 *      Author: Ryohei
 */

#ifndef GERGE_H_
#define GERGE_H_

#include "stm32f4xx_hal.h"
#include "tim.h"

//定数
#define GERGE_VEL_MAP_LONG		500			//500mmまで
#define GERGE_VEL_MAP_INTERVAL	0.001f		//1mm
#define GERGE_POS_MAP_LONG		500			//5秒まで
#define GERGE_POS_MAP_INTERVAL	0.01f		//10ms

#define GERGE_POS_TOP			0.35f
#define GERGE_POS_BOTTOM		0.0f
#define GERGE_POS_UUKHAI		0.30f
#define GERGE_POS_TRANSFER		0.08f

//グローバル変数
extern int16_t gerge_trapezoid_end_time;
extern float gerge_vel_map[GERGE_VEL_MAP_LONG];
extern float gerge_pos_map[GERGE_POS_MAP_LONG];

//プロトタイプ宣言
void GergeCatchStart(void);
void GergeCatchAndWaitButton(void);
void GergeUukhai(void);
void GergeReturnToBottom(void);

void GergeTrapezoidMove(float, float, float, float);
void GergeGetZeroPos(void);
void GergeMotorSetPos(float);

void GergeMotorSetDuty(float duty);
void GergeMotorEnable(void);
void GergeMotorDisable(void);

int8_t GergeLimitIsOn(void);

void GergeHandClose(void);
void GergeHandOpen(void);

int8_t GergeIsToCatch(void);

//レールDCモータ
#define GERGE_MOTOR_TIM_HANDLER			htim8
#define GERGE_MOTOR_TIM_CH				TIM_CHANNEL_3
#define GERGE_MOTOR_DIR_1_PORT			GPIOB
#define GERGE_MOTOR_DIR_1_PIN			GPIO_PIN_14
#define GERGE_MOTOR_DIR_2_PORT			GPIOB
#define GERGE_MOTOR_DIR_2_PIN			GPIO_PIN_15
//PWM周波数
#define GERGE_PWM_FREQ					10000
//タイマ周波数
#define GERGE_TIM_FREQ					72000000
//Duty比のリミット
#define GERGE_DUTY_LIMIT				0.50f

//レールリミットスイッチ
#define GERGE_LIMIT_SW_PORT				GPIOB
#define GERGE_LIMIT_SW_PIN				GPIO_PIN_7

//ハンドエアシリンダ
#define GERGE_HAND_CYLINDER_PORT		GPIOA
#define GERGE_HAND_CYLINDER_PIN			GPIO_PIN_11

//接触センサ
#define GERGE_TOUCH_SW_PORT				GPIOB
#define GERGE_TOUCH_SW_PIN				GPIO_PIN_6


#endif /* GERGE_H_ */
