
/*
 * trajectory.h
 *
 *  Created on: 2018/12/28
 *      Author: nabeya11
 */

#ifndef PATH_H_
#define PATH_H_


#include <adcprocess.h>
#include "stm32f4xx_hal.h"
#include "robotstate.h"
#include "user_interface.h"
#include "mode.h"

typedef enum {
	PF_FOLLOWING,
	PF_FINISH
} pure_pursuit_status_e;

#define MAX_STRAIGHT_PATH_LENGTH	1000

//座標
#define START_X				-5.250f
#define START_Y				7.800f

#define BEFORE_SANDDUNE_X	-5.250f
#define BEFORE_SANDDUNE_Y	4.800f

#define BEFORE_TUSSOCK_X	-3.500f
#define BEFORE_TUSSOCK_Y	3.300f

//経路情報
extern pos_s start_to_sanddune[355];
extern pos_s sanddune_to_tussock[180];
extern pos_s straight_path[MAX_STRAIGHT_PATH_LENGTH];

//プロトタイプ宣言
void PrepareNextPath(field_select_e, wall_sens_s);
pure_pursuit_status_e CalcPurePursuit(state_s *, field_select_e);


#endif /* PATH_H_ */
