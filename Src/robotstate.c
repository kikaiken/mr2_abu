/*
 * robotstate.c
 *
 *  Created on: 2018/06/21
 *      Author: Ryohei
 */

#include "robotstate.h"
#include "constant.h"
#include "MadgwickAHRS.h"
#include "light_math.h"
#include "user_interface.h"
#include "gerge.h"
#include "leg.h"
#include <math.h>

state_s state;
pos_s integ_pos;
volatile float walk_speed;

/*
 * オドメトリを計算する
 * @param	imu : MPU9250のデータ
 * @return
 */
void GetOdometry(imu_t imu) {
//	static angle_s acc_ang = {};

	//水平面内での自己位置推定
	state.angvel = imu.gyro_z;
	state.odom.theta += state.angvel * CTRL_CYCLE;
	state.odom.x += state.vel * cosf(state.odom.theta) * CTRL_CYCLE;
	state.odom.y += state.vel * sinf(state.odom.theta) * CTRL_CYCLE;
}

void SetMoveMode(move_mode_e mode) {
	state.move_mode = mode;
}

void MR2CtrlStart(void) {
	SetMoveMode(MM_STOP);
	for (int8_t i = 0; i < 20; i++) {MotorSetSpdTurnDuty(0, 0);}
	GergeMotorEnable();
	HAL_TIM_Base_Start_IT(&htim6);
}

void MR2CtrlStop(void) {
	HAL_TIM_Base_Stop_IT(&htim6);
	for (int8_t i = 0; i < 20; i++) {MotorSetSpdTurnDuty(0, 0);}
	GergeMotorDisable();
	BuzzerOff();
}
