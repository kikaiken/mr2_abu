/*
 * path.c
 *
 *  Created on: 2019/03/10
 *      Author: Ryohei
 */

#include "path.h"
#include <stdio.h>
#include <math.h>
#include "light_math.h"
#include "constant.h"

//定数
//#define DEBUG

#define PATH_DATA_INTERVAL		0.01f

//ファイル内変数
static pos_s *path;
static size_t path_long_cm;
static size_t straight_path_long_cm;
static float PF_PGAIN;

//static関数プロトタイプ宣言
static void MakeStraightPath(pos_s, pos_s);
static void ClearStraightPath(void);

/*
 * PREPAREモード時に経路切替・速度マップ生成を行い、走行モードに移行
 * @param	field_color : FIELD_RED / FIELD_BLUE
 * 			wall_sens : 壁センサ値
 * @return
 */
void PrepareNextPath(field_select_e field_color, wall_sens_s wall_sens) {
	pos_s now, start, goal;
	switch (state.move_mode){
	case MM_START_TO_SANDDUNE_PREPARE :
		//現在位置指定（木枠見て推定？or治具で固定）
		state.odom.x = ((field_color == FIELD_RED) ? (START_X) : (-START_X));
		state.odom.y = START_Y;
//		state.odom.theta = 3 * M_PI / 2;

		//始点・終点設定
		start.x = START_X;
		start.y = START_Y;
		goal.x  = BEFORE_SANDDUNE_X;
		goal.y  = BEFORE_SANDDUNE_Y;

		//経路生成
		MakeStraightPath(start, goal);
		path = straight_path;
		path_long_cm = straight_path_long_cm;

		//ゲイン設定
		PF_PGAIN = 3.0;

		//準備完了
		SetMoveMode(MM_START_TO_SANDDUNE);
		break;
	case MM_SANDDUNE_TO_TUSSOCK_PREPARE :
		//現在位置指定（木枠見て推定）
		if (field_color == FIELD_RED) {		//赤
			now.x = -4.0f - ((wall_sens.left_rear + 0.25f) / sqrtf(2));
			now.y =  4.0f - ((wall_sens.left_rear + 0.25f) / sqrtf(2));
		} else {							//青
			now.x =  4.0f + ((wall_sens.right_rear + 0.25f) / sqrtf(2));
			now.y =  4.0f - ((wall_sens.right_rear + 0.25f) / sqrtf(2));
		}

		state.odom.x = now.x;
		state.odom.y = now.y;
		state.odom.theta = ((field_color == FIELD_RED) ? (7 * M_PI / 4) : (5 * M_PI / 4));

		//始点・終点設定
		start.x = ((field_color == FIELD_RED) ? (now.x) : (-now.x));
		start.y = now.y;
		goal.x  = BEFORE_TUSSOCK_X;
		goal.y  = BEFORE_TUSSOCK_Y;

		//現在角度指定
		state.odom.theta = ((field_color == FIELD_RED) ? (7 * M_PI / 4) : (5 * M_PI / 4));

		//経路生成
		MakeStraightPath(start, goal);
		path = straight_path;
		path_long_cm = straight_path_long_cm;

		//ゲイン設定
		PF_PGAIN = 3.0;

		//準備完了
		SetMoveMode(MM_SANDDUNE_TO_TUSSOCK);
		break;
	default : break;
	}
}


/*
 * Pure Pursuit
 * @param	ref : stateの目標
 * 			field_color : FIELD_RED / FIELD_BLUE
 * @return	PF_FOLLOWING(0) : 経路追従中
 * 			PF_FINISH(1)    : 経路追従終了
*/
pure_pursuit_status_e CalcPurePursuit(state_s *ref, field_select_e field_color) {
	static int32_t nearest_index = 0;		//現在位置から最も近い経路上の点
	int32_t reference_index = 0;			//Pure Pursuitにおける目標点
	float length = 0, min_length = 1;
	float dx, dy, theta, theta_sign, theta_abs;						//目標点と現在位置の角度差

	//nearest_indexの探索
	for (int32_t i = MAX(nearest_index - 15, 0); i < MIN(nearest_index + 15, path_long_cm); i++) {
		if (field_color == FIELD_RED) {length = hypotf((integ_pos.x -   path[i].x),  (integ_pos.y - path[i].y));}
		else						  {length = hypotf((integ_pos.x - (-path[i].x)), (integ_pos.y - path[i].y));}
		if (length < min_length) {
			nearest_index = MAX(i, 2);
			min_length = length;
		}
	}

	//reference_indexを決定
	reference_index = MIN(nearest_index + 50, path_long_cm - 1);

	//目標位置と現在位置の角度差を求める
	if (field_color == FIELD_RED) {dx =  path[reference_index].x - integ_pos.x;}
	else						  {dx = -path[reference_index].x - integ_pos.x;}
	dy = path[reference_index].y - integ_pos.y;
	//角度差
	theta = atan2f(dy, dx) - state.odom.theta;
	theta_sign = signf(theta);
	theta_abs  = fabsf(theta);

	while (fabsf(theta_abs) > M_PI) {theta_abs -= 2 * M_PI;}
	theta = theta_sign * theta_abs;

	//角速度を決定
	ref->angvel = theta * PF_PGAIN;

#ifdef DEBUG
	//デバッグモード
//	integ_pos.x += 0.50f * cosf(state.odom.theta) * CTRL_CYCLE;
//	integ_pos.y += 0.50f * sinf(state.odom.theta) * CTRL_CYCLE;
//	state.odom.theta += ref->angvel * CTRL_CYCLE;
//	printf("%f %f %f\n", integ_pos.x, integ_pos.y, state.odom.theta);
#endif

	//終了判定
	if (nearest_index < path_long_cm - 10) {
		return PF_FOLLOWING;
	} else {
		nearest_index = 0;
		return PF_FINISH;
	}
}

/*
 * 2点間を直線で結ぶ経路を生成
 * @param
 * @return
 */
static void MakeStraightPath(pos_s start, pos_s goal) {
	//配列クリア
	ClearStraightPath();

	//経路生成
	float theta;
	straight_path[0] = start;	//始点を設定
	theta = atan2f(goal.y - start.y, goal.x - start.x);		//直線の傾き
	straight_path_long_cm = fminf(hypotf(goal.x - start.x, goal.y - start.y) / PATH_DATA_INTERVAL, MAX_STRAIGHT_PATH_LENGTH);	//スタートからゴールまでの距離(cm)

	//1cmごとの点を生成
	for (int32_t i = 1; i < straight_path_long_cm; i++) {
		straight_path[i].x = straight_path[i - 1].x + PATH_DATA_INTERVAL * cosf(theta);
		straight_path[i].y = straight_path[i - 1].y + PATH_DATA_INTERVAL * sinf(theta);
	}

#ifdef DEBUG
	printf("直線経路開始\n");
	printf("経路長 : %d [cm]\n", straight_path_long_cm);
	for (int32_t i = 0; i < straight_path_long_cm; i++) {
		printf("%f %f\n", straight_path[i].x, straight_path[i].y);
	}
	printf("直線経路終了\n");
#endif
}

/*
 * 直線経路の配列をクリア
 * @param
 * @return
 */
static void ClearStraightPath(void) {
	for (int32_t i = 0; i < MAX_STRAIGHT_PATH_LENGTH; i++) {
		straight_path[i].x = straight_path[i].y = straight_path[i].theta = 0;
	}
}

//経路情報
pos_s start_to_sanddune[355] = {
{	-5.47500	,	7.80000	,	0	},
{	-5.47480	,	7.79044	,	0	},
{	-5.47459	,	7.78023	,	0	},
{	-5.47438	,	7.77003	,	0	},
{	-5.47417	,	7.75986	,	0	},
{	-5.47397	,	7.74972	,	0	},
{	-5.47376	,	7.73959	,	0	},
{	-5.47356	,	7.72949	,	0	},
{	-5.47336	,	7.71942	,	0	},
{	-5.47315	,	7.70936	,	0	},
{	-5.47295	,	7.69933	,	0	},
{	-5.47276	,	7.68933	,	0	},
{	-5.47256	,	7.67934	,	0	},
{	-5.47236	,	7.66938	,	0	},
{	-5.47217	,	7.65944	,	0	},
{	-5.47197	,	7.64953	,	0	},
{	-5.47177	,	7.63897	,	0	},
{	-5.47157	,	7.62845	,	0	},
{	-5.47136	,	7.61795	,	0	},
{	-5.47116	,	7.60748	,	0	},
{	-5.47096	,	7.59703	,	0	},
{	-5.47077	,	7.58661	,	0	},
{	-5.47057	,	7.57621	,	0	},
{	-5.47037	,	7.56585	,	0	},
{	-5.47018	,	7.55550	,	0	},
{	-5.46999	,	7.54518	,	0	},
{	-5.46980	,	7.53489	,	0	},
{	-5.46961	,	7.52463	,	0	},
{	-5.46942	,	7.51439	,	0	},
{	-5.46923	,	7.50417	,	0	},
{	-5.46905	,	7.49398	,	0	},
{	-5.46887	,	7.48382	,	0	},
{	-5.46868	,	7.47368	,	0	},
{	-5.46850	,	7.46357	,	0	},
{	-5.46832	,	7.45348	,	0	},
{	-5.46814	,	7.44342	,	0	},
{	-5.46797	,	7.43338	,	0	},
{	-5.46779	,	7.42337	,	0	},
{	-5.46762	,	7.41339	,	0	},
{	-5.46744	,	7.40343	,	0	},
{	-5.46727	,	7.39349	,	0	},
{	-5.46710	,	7.38358	,	0	},
{	-5.46692	,	7.37308	,	0	},
{	-5.46674	,	7.36261	,	0	},
{	-5.46657	,	7.35216	,	0	},
{	-5.46639	,	7.34175	,	0	},
{	-5.46622	,	7.33136	,	0	},
{	-5.46604	,	7.32100	,	0	},
{	-5.46587	,	7.31066	,	0	},
{	-5.46570	,	7.30036	,	0	},
{	-5.46554	,	7.29009	,	0	},
{	-5.46537	,	7.27984	,	0	},
{	-5.46520	,	7.26962	,	0	},
{	-5.46504	,	7.25943	,	0	},
{	-5.46488	,	7.24926	,	0	},
{	-5.46472	,	7.23913	,	0	},
{	-5.46456	,	7.22902	,	0	},
{	-5.46440	,	7.21894	,	0	},
{	-5.46424	,	7.20889	,	0	},
{	-5.46409	,	7.19886	,	0	},
{	-5.46393	,	7.18886	,	0	},
{	-5.46378	,	7.17889	,	0	},
{	-5.46363	,	7.16895	,	0	},
{	-5.46348	,	7.15904	,	0	},
{	-5.46332	,	7.14857	,	0	},
{	-5.46316	,	7.13813	,	0	},
{	-5.46301	,	7.12773	,	0	},
{	-5.46285	,	7.11735	,	0	},
{	-5.46270	,	7.10700	,	0	},
{	-5.46255	,	7.09669	,	0	},
{	-5.46240	,	7.08641	,	0	},
{	-5.46226	,	7.07615	,	0	},
{	-5.46211	,	7.06593	,	0	},
{	-5.46196	,	7.05574	,	0	},
{	-5.46182	,	7.04557	,	0	},
{	-5.46168	,	7.03544	,	0	},
{	-5.46154	,	7.02534	,	0	},
{	-5.46140	,	7.01527	,	0	},
{	-5.46126	,	7.00523	,	0	},
{	-5.46113	,	6.99521	,	0	},
{	-5.46099	,	6.98523	,	0	},
{	-5.46086	,	6.97528	,	0	},
{	-5.46072	,	6.96536	,	0	},
{	-5.46059	,	6.95492	,	0	},
{	-5.46045	,	6.94451	,	0	},
{	-5.46031	,	6.93413	,	0	},
{	-5.46018	,	6.92379	,	0	},
{	-5.46005	,	6.91348	,	0	},
{	-5.45992	,	6.90321	,	0	},
{	-5.45979	,	6.89297	,	0	},
{	-5.45966	,	6.88276	,	0	},
{	-5.45953	,	6.87258	,	0	},
{	-5.45941	,	6.86243	,	0	},
{	-5.45928	,	6.85232	,	0	},
{	-5.45916	,	6.84224	,	0	},
{	-5.45904	,	6.83220	,	0	},
{	-5.45892	,	6.82218	,	0	},
{	-5.45880	,	6.81220	,	0	},
{	-5.45868	,	6.80225	,	0	},
{	-5.45857	,	6.79233	,	0	},
{	-5.45845	,	6.78193	,	0	},
{	-5.45833	,	6.77156	,	0	},
{	-5.45821	,	6.76122	,	0	},
{	-5.45809	,	6.75093	,	0	},
{	-5.45798	,	6.74066	,	0	},
{	-5.45786	,	6.73044	,	0	},
{	-5.45775	,	6.72024	,	0	},
{	-5.45764	,	6.71009	,	0	},
{	-5.45753	,	6.69997	,	0	},
{	-5.45742	,	6.68988	,	0	},
{	-5.45732	,	6.67983	,	0	},
{	-5.45721	,	6.66981	,	0	},
{	-5.45711	,	6.65983	,	0	},
{	-5.45701	,	6.64988	,	0	},
{	-5.45691	,	6.63997	,	0	},
{	-5.45680	,	6.62960	,	0	},
{	-5.45670	,	6.61927	,	0	},
{	-5.45659	,	6.60897	,	0	},
{	-5.45649	,	6.59872	,	0	},
{	-5.45639	,	6.58850	,	0	},
{	-5.45630	,	6.57832	,	0	},
{	-5.45620	,	6.56817	,	0	},
{	-5.45610	,	6.55807	,	0	},
{	-5.45601	,	6.54800	,	0	},
{	-5.45592	,	6.53797	,	0	},
{	-5.45583	,	6.52798	,	0	},
{	-5.45574	,	6.51802	,	0	},
{	-5.45565	,	6.50810	,	0	},
{	-5.45555	,	6.49775	,	0	},
{	-5.45546	,	6.48744	,	0	},
{	-5.45537	,	6.47717	,	0	},
{	-5.45529	,	6.46694	,	0	},
{	-5.45520	,	6.45675	,	0	},
{	-5.45512	,	6.44660	,	0	},
{	-5.45503	,	6.43649	,	0	},
{	-5.45495	,	6.42642	,	0	},
{	-5.45487	,	6.41639	,	0	},
{	-5.45479	,	6.40641	,	0	},
{	-5.45471	,	6.39646	,	0	},
{	-5.45463	,	6.38655	,	0	},
{	-5.45455	,	6.37623	,	0	},
{	-5.45447	,	6.36596	,	0	},
{	-5.45440	,	6.35572	,	0	},
{	-5.45432	,	6.34554	,	0	},
{	-5.45424	,	6.33539	,	0	},
{	-5.45417	,	6.32529	,	0	},
{	-5.45410	,	6.31523	,	0	},
{	-5.45403	,	6.30521	,	0	},
{	-5.45396	,	6.29524	,	0	},
{	-5.45389	,	6.28530	,	0	},
{	-5.45382	,	6.27499	,	0	},
{	-5.45375	,	6.26471	,	0	},
{	-5.45368	,	6.25448	,	0	},
{	-5.45361	,	6.24430	,	0	},
{	-5.45355	,	6.23417	,	0	},
{	-5.45348	,	6.22407	,	0	},
{	-5.45342	,	6.21403	,	0	},
{	-5.45336	,	6.20403	,	0	},
{	-5.45330	,	6.19407	,	0	},
{	-5.45323	,	6.18416	,	0	},
{	-5.45317	,	6.17388	,	0	},
{	-5.45311	,	6.16366	,	0	},
{	-5.45305	,	6.15348	,	0	},
{	-5.45299	,	6.14335	,	0	},
{	-5.45294	,	6.13326	,	0	},
{	-5.45288	,	6.12323	,	0	},
{	-5.45283	,	6.11324	,	0	},
{	-5.45277	,	6.10330	,	0	},
{	-5.45272	,	6.09301	,	0	},
{	-5.45266	,	6.08278	,	0	},
{	-5.45261	,	6.07259	,	0	},
{	-5.45255	,	6.06246	,	0	},
{	-5.45250	,	6.05237	,	0	},
{	-5.45245	,	6.04234	,	0	},
{	-5.45240	,	6.03236	,	0	},
{	-5.45235	,	6.02243	,	0	},
{	-5.45230	,	6.01216	,	0	},
{	-5.45225	,	6.00196	,	0	},
{	-5.45221	,	5.99180	,	0	},
{	-5.45216	,	5.98170	,	0	},
{	-5.45211	,	5.97166	,	0	},
{	-5.45207	,	5.96166	,	0	},
{	-5.45202	,	5.95172	,	0	},
{	-5.45198	,	5.94147	,	0	},
{	-5.45193	,	5.93127	,	0	},
{	-5.45189	,	5.92113	,	0	},
{	-5.45184	,	5.91105	,	0	},
{	-5.45180	,	5.90102	,	0	},
{	-5.45176	,	5.89105	,	0	},
{	-5.45171	,	5.88114	,	0	},
{	-5.45167	,	5.87092	,	0	},
{	-5.45163	,	5.86077	,	0	},
{	-5.45159	,	5.85068	,	0	},
{	-5.45155	,	5.84064	,	0	},
{	-5.45151	,	5.83067	,	0	},
{	-5.45146	,	5.82075	,	0	},
{	-5.45142	,	5.81055	,	0	},
{	-5.45138	,	5.80042	,	0	},
{	-5.45134	,	5.79034	,	0	},
{	-5.45130	,	5.78033	,	0	},
{	-5.45126	,	5.77038	,	0	},
{	-5.45122	,	5.76016	,	0	},
{	-5.45118	,	5.75000	,	0	},
{	-5.45114	,	5.73991	,	0	},
{	-5.45110	,	5.72989	,	0	},
{	-5.45106	,	5.71993	,	0	},
{	-5.45101	,	5.70971	,	0	},
{	-5.45097	,	5.69957	,	0	},
{	-5.45093	,	5.68949	,	0	},
{	-5.45089	,	5.67948	,	0	},
{	-5.45085	,	5.66953	,	0	},
{	-5.45080	,	5.65934	,	0	},
{	-5.45076	,	5.64923	,	0	},
{	-5.45071	,	5.63918	,	0	},
{	-5.45067	,	5.62921	,	0	},
{	-5.45062	,	5.61930	,	0	},
{	-5.45058	,	5.60917	,	0	},
{	-5.45053	,	5.59911	,	0	},
{	-5.45048	,	5.58912	,	0	},
{	-5.45043	,	5.57921	,	0	},
{	-5.45038	,	5.56908	,	0	},
{	-5.45033	,	5.55902	,	0	},
{	-5.45028	,	5.54904	,	0	},
{	-5.45023	,	5.53914	,	0	},
{	-5.45017	,	5.52903	,	0	},
{	-5.45011	,	5.51900	,	0	},
{	-5.45005	,	5.50905	,	0	},
{	-5.44999	,	5.49891	,	0	},
{	-5.44993	,	5.48884	,	0	},
{	-5.44987	,	5.47886	,	0	},
{	-5.44980	,	5.46869	,	0	},
{	-5.44973	,	5.45861	,	0	},
{	-5.44966	,	5.44862	,	0	},
{	-5.44959	,	5.43870	,	0	},
{	-5.44951	,	5.42862	,	0	},
{	-5.44943	,	5.41862	,	0	},
{	-5.44935	,	5.40870	,	0	},
{	-5.44927	,	5.39863	,	0	},
{	-5.44918	,	5.38864	,	0	},
{	-5.44909	,	5.37850	,	0	},
{	-5.44899	,	5.36845	,	0	},
{	-5.44889	,	5.35850	,	0	},
{	-5.44879	,	5.34840	,	0	},
{	-5.44868	,	5.33839	,	0	},
{	-5.44857	,	5.32848	,	0	},
{	-5.44846	,	5.31844	,	0	},
{	-5.44834	,	5.30849	,	0	},
{	-5.44821	,	5.29841	,	0	},
{	-5.44808	,	5.28844	,	0	},
{	-5.44794	,	5.27835	,	0	},
{	-5.44780	,	5.26836	,	0	},
{	-5.44765	,	5.25825	,	0	},
{	-5.44749	,	5.24826	,	0	},
{	-5.44733	,	5.23816	,	0	},
{	-5.44716	,	5.22817	,	0	},
{	-5.44698	,	5.21808	,	0	},
{	-5.44679	,	5.20811	,	0	},
{	-5.44659	,	5.19805	,	0	},
{	-5.44639	,	5.18810	,	0	},
{	-5.44618	,	5.17807	,	0	},
{	-5.44596	,	5.16816	,	0	},
{	-5.44572	,	5.15817	,	0	},
{	-5.44548	,	5.14812	,	0	},
{	-5.44522	,	5.13819	,	0	},
{	-5.44495	,	5.12820	,	0	},
{	-5.44467	,	5.11815	,	0	},
{	-5.44438	,	5.10823	,	0	},
{	-5.44407	,	5.09826	,	0	},
{	-5.44375	,	5.08825	,	0	},
{	-5.44340	,	5.07821	,	0	},
{	-5.44305	,	5.06830	,	0	},
{	-5.44268	,	5.05836	,	0	},
{	-5.44228	,	5.04840	,	0	},
{	-5.44187	,	5.03841	,	0	},
{	-5.44144	,	5.02841	,	0	},
{	-5.44098	,	5.01840	,	0	},
{	-5.44050	,	5.00839	,	0	},
{	-5.43999	,	4.99838	,	0	},
{	-5.43946	,	4.98838	,	0	},
{	-5.43890	,	4.97839	,	0	},
{	-5.43831	,	4.96842	,	0	},
{	-5.43769	,	4.95847	,	0	},
{	-5.43704	,	4.94854	,	0	},
{	-5.43636	,	4.93865	,	0	},
{	-5.43563	,	4.92866	,	0	},
{	-5.43487	,	4.91871	,	0	},
{	-5.43407	,	4.90881	,	0	},
{	-5.43321	,	4.89883	,	0	},
{	-5.43232	,	4.88892	,	0	},
{	-5.43137	,	4.87894	,	0	},
{	-5.43037	,	4.86904	,	0	},
{	-5.42930	,	4.85910	,	0	},
{	-5.42819	,	4.84924	,	0	},
{	-5.42700	,	4.83935	,	0	},
{	-5.42575	,	4.82945	,	0	},
{	-5.42441	,	4.81953	,	0	},
{	-5.42302	,	4.80972	,	0	},
{	-5.42153	,	4.79991	,	0	},
{	-5.41996	,	4.79011	,	0	},
{	-5.41829	,	4.78033	,	0	},
{	-5.41653	,	4.77056	,	0	},
{	-5.41466	,	4.76083	,	0	},
{	-5.41268	,	4.75112	,	0	},
{	-5.41056	,	4.74136	,	0	},
{	-5.40832	,	4.73164	,	0	},
{	-5.40595	,	4.72197	,	0	},
{	-5.40344	,	4.71234	,	0	},
{	-5.40079	,	4.70276	,	0	},
{	-5.39799	,	4.69322	,	0	},
{	-5.39503	,	4.68372	,	0	},
{	-5.39190	,	4.67426	,	0	},
{	-5.38863	,	4.66491	,	0	},
{	-5.38517	,	4.65558	,	0	},
{	-5.38155	,	4.64635	,	0	},
{	-5.37777	,	4.63719	,	0	},
{	-5.37381	,	4.62809	,	0	},
{	-5.36968	,	4.61905	,	0	},
{	-5.36539	,	4.61011	,	0	},
{	-5.36095	,	4.60126	,	0	},
{	-5.35636	,	4.59249	,	0	},
{	-5.35160	,	4.58376	,	0	},
{	-5.34673	,	4.57514	,	0	},
{	-5.34169	,	4.56654	,	0	},
{	-5.33653	,	4.55801	,	0	},
{	-5.33129	,	4.54960	,	0	},
{	-5.32593	,	4.54123	,	0	},
{	-5.32046	,	4.53288	,	0	},
{	-5.31493	,	4.52461	,	0	},
{	-5.30933	,	4.51641	,	0	},
{	-5.30368	,	4.50827	,	0	},
{	-5.29792	,	4.50010	,	0	},
{	-5.29213	,	4.49198	,	0	},
{	-5.28629	,	4.48390	,	0	},
{	-5.28042	,	4.47584	,	0	},
{	-5.27452	,	4.46781	,	0	},
{	-5.26860	,	4.45980	,	0	},
{	-5.26266	,	4.45182	,	0	},
{	-5.25672	,	4.44386	,	0	},
{	-5.25077	,	4.43591	,	0	},
{	-5.24483	,	4.42799	,	0	},
{	-5.23883	,	4.42001	,	0	},
{	-5.23285	,	4.41204	,	0	},
{	-5.22688	,	4.40411	,	0	},
{	-5.22088	,	4.39611	,	0	},
{	-5.21490	,	4.38814	,	0	},
{	-5.20897	,	4.38020	,	0	},
{	-5.20300	,	4.37221	,	0	},
{	-5.19708	,	4.36426	,	0	},
{	-5.19114	,	4.35625	,	0	},
{	-5.18518	,	4.34820	,	0	},
{	-5.17928	,	4.34019	,	0	},
{	-5.17337	,	4.33215	,	0	},
{	-5.16745	,	4.32406	,	0	},
{	-5.16161	,	4.31603	,	0	},
{	-5.15576	,	4.30798	,	0	}
};

pos_s sanddune_to_tussock[180] = {
{	-4.50000	,	3.65000	,	0	},
{	-4.49254	,	3.64364	,	0	},
{	-4.48489	,	3.63719	,	0	},
{	-4.47722	,	3.63079	,	0	},
{	-4.46954	,	3.62445	,	0	},
{	-4.46185	,	3.61816	,	0	},
{	-4.45415	,	3.61193	,	0	},
{	-4.44636	,	3.60568	,	0	},
{	-4.43857	,	3.59950	,	0	},
{	-4.43080	,	3.59337	,	0	},
{	-4.42294	,	3.58723	,	0	},
{	-4.41509	,	3.58115	,	0	},
{	-4.40717	,	3.57506	,	0	},
{	-4.39928	,	3.56903	,	0	},
{	-4.39131	,	3.56300	,	0	},
{	-4.38337	,	3.55703	,	0	},
{	-4.37536	,	3.55105	,	0	},
{	-4.36739	,	3.54514	,	0	},
{	-4.35935	,	3.53922	,	0	},
{	-4.35136	,	3.53336	,	0	},
{	-4.34330	,	3.52750	,	0	},
{	-4.33520	,	3.52164	,	0	},
{	-4.32714	,	3.51585	,	0	},
{	-4.31902	,	3.51005	,	0	},
{	-4.31086	,	3.50424	,	0	},
{	-4.30276	,	3.49851	,	0	},
{	-4.29460	,	3.49278	,	0	},
{	-4.28640	,	3.48704	,	0	},
{	-4.27827	,	3.48137	,	0	},
{	-4.27009	,	3.47571	,	0	},
{	-4.26187	,	3.47004	,	0	},
{	-4.25361	,	3.46438	,	0	},
{	-4.24543	,	3.45878	,	0	},
{	-4.23720	,	3.45319	,	0	},
{	-4.22895	,	3.44760	,	0	},
{	-4.22066	,	3.44201	,	0	},
{	-4.21234	,	3.43642	,	0	},
{	-4.20409	,	3.43090	,	0	},
{	-4.19582	,	3.42539	,	0	},
{	-4.18752	,	3.41988	,	0	},
{	-4.17920	,	3.41438	,	0	},
{	-4.17085	,	3.40888	,	0	},
{	-4.16248	,	3.40338	,	0	},
{	-4.15420	,	3.39796	,	0	},
{	-4.14589	,	3.39255	,	0	},
{	-4.13756	,	3.38714	,	0	},
{	-4.12922	,	3.38174	,	0	},
{	-4.12086	,	3.37634	,	0	},
{	-4.11248	,	3.37095	,	0	},
{	-4.10409	,	3.36557	,	0	},
{	-4.09569	,	3.36019	,	0	},
{	-4.08727	,	3.35482	,	0	},
{	-4.07884	,	3.34946	,	0	},
{	-4.07040	,	3.34411	,	0	},
{	-4.06195	,	3.33876	,	0	},
{	-4.05349	,	3.33343	,	0	},
{	-4.04503	,	3.32811	,	0	},
{	-4.03656	,	3.32279	,	0	},
{	-4.02809	,	3.31749	,	0	},
{	-4.01961	,	3.31219	,	0	},
{	-4.01113	,	3.30691	,	0	},
{	-4.00264	,	3.30164	,	0	},
{	-3.99416	,	3.29638	,	0	},
{	-3.98568	,	3.29113	,	0	},
{	-3.97720	,	3.28590	,	0	},
{	-3.96872	,	3.28068	,	0	},
{	-3.96025	,	3.27547	,	0	},
{	-3.95178	,	3.27027	,	0	},
{	-3.94332	,	3.26509	,	0	},
{	-3.93487	,	3.25992	,	0	},
{	-3.92631	,	3.25471	,	0	},
{	-3.91777	,	3.24950	,	0	},
{	-3.90923	,	3.24431	,	0	},
{	-3.90071	,	3.23914	,	0	},
{	-3.89220	,	3.23399	,	0	},
{	-3.88371	,	3.22885	,	0	},
{	-3.87523	,	3.22373	,	0	},
{	-3.86666	,	3.21856	,	0	},
{	-3.85811	,	3.21341	,	0	},
{	-3.84958	,	3.20828	,	0	},
{	-3.84107	,	3.20317	,	0	},
{	-3.83247	,	3.19801	,	0	},
{	-3.82390	,	3.19288	,	0	},
{	-3.81535	,	3.18776	,	0	},
{	-3.80683	,	3.18267	,	0	},
{	-3.79824	,	3.17753	,	0	},
{	-3.78967	,	3.17242	,	0	},
{	-3.78113	,	3.16733	,	0	},
{	-3.77252	,	3.16221	,	0	},
{	-3.76395	,	3.15711	,	0	},
{	-3.75541	,	3.15203	,	0	},
{	-3.74680	,	3.14692	,	0	},
{	-3.73823	,	3.14183	,	0	},
{	-3.72971	,	3.13677	,	0	},
{	-3.72112	,	3.13168	,	0	},
{	-3.71258	,	3.12662	,	0	},
{	-3.70399	,	3.12152	,	0	},
{	-3.69544	,	3.11646	,	0	},
{	-3.68684	,	3.11137	,	0	},
{	-3.67830	,	3.10631	,	0	},
{	-3.66971	,	3.10122	,	0	},
{	-3.66118	,	3.09617	,	0	},
{	-3.65262	,	3.09110	,	0	},
{	-3.64401	,	3.08601	,	0	},
{	-3.63548	,	3.08095	,	0	},
{	-3.62691	,	3.07587	,	0	},
{	-3.61832	,	3.07078	,	0	},
{	-3.60972	,	3.06567	,	0	},
{	-3.60119	,	3.06061	,	0	},
{	-3.59264	,	3.05553	,	0	},
{	-3.58409	,	3.05045	,	0	},
{	-3.57554	,	3.04535	,	0	},
{	-3.56699	,	3.04025	,	0	},
{	-3.55844	,	3.03515	,	0	},
{	-3.54991	,	3.03005	,	0	},
{	-3.54139	,	3.02495	,	0	},
{	-3.53290	,	3.01986	,	0	},
{	-3.52435	,	3.01472	,	0	},
{	-3.51585	,	3.00959	,	0	},
{	-3.50730	,	3.00442	,	0	},
{	-3.49881	,	2.99928	,	0	},
{	-3.49029	,	2.99410	,	0	},
{	-3.48177	,	2.98890	,	0	},
{	-3.47331	,	2.98372	,	0	},
{	-3.46486	,	2.97851	,	0	},
{	-3.45641	,	2.97328	,	0	},
{	-3.44797	,	2.96804	,	0	},
{	-3.43955	,	2.96276	,	0	},
{	-3.43116	,	2.95747	,	0	},
{	-3.42274	,	2.95212	,	0	},
{	-3.41437	,	2.94675	,	0	},
{	-3.40604	,	2.94136	,	0	},
{	-3.39772	,	2.93591	,	0	},
{	-3.38946	,	2.93044	,	0	},
{	-3.38122	,	2.92491	,	0	},
{	-3.37301	,	2.91932	,	0	},
{	-3.36484	,	2.91367	,	0	},
{	-3.35672	,	2.90797	,	0	},
{	-3.34867	,	2.90220	,	0	},
{	-3.34065	,	2.89632	,	0	},
{	-3.33271	,	2.89038	,	0	},
{	-3.32484	,	2.88434	,	0	},
{	-3.31703	,	2.87818	,	0	},
{	-3.30932	,	2.87191	,	0	},
{	-3.30171	,	2.86552	,	0	},
{	-3.29423	,	2.85901	,	0	},
{	-3.28689	,	2.85236	,	0	},
{	-3.27967	,	2.84554	,	0	},
{	-3.27263	,	2.83857	,	0	},
{	-3.26575	,	2.83139	,	0	},
{	-3.25909	,	2.82404	,	0	},
{	-3.25266	,	2.81651	,	0	},
{	-3.24648	,	2.80877	,	0	},
{	-3.24057	,	2.80080	,	0	},
{	-3.23497	,	2.79264	,	0	},
{	-3.22969	,	2.78425	,	0	},
{	-3.22476	,	2.77567	,	0	},
{	-3.22018	,	2.76686	,	0	},
{	-3.21598	,	2.75784	,	0	},
{	-3.21218	,	2.74869	,	0	},
{	-3.20878	,	2.73939	,	0	},
{	-3.20577	,	2.72993	,	0	},
{	-3.20314	,	2.72037	,	0	},
{	-3.20089	,	2.71073	,	0	},
{	-3.19899	,	2.70098	,	0	},
{	-3.19744	,	2.69118	,	0	},
{	-3.19619	,	2.68133	,	0	},
{	-3.19525	,	2.67147	,	0	},
{	-3.19457	,	2.66157	,	0	},
{	-3.19415	,	2.65163	,	0	},
{	-3.19395	,	2.64171	,	0	},
{	-3.19397	,	2.63176	,	0	},
{	-3.19418	,	2.62186	,	0	},
{	-3.19455	,	2.61194	,	0	},
{	-3.19509	,	2.60203	,	0	},
{	-3.19577	,	2.59213	,	0	},
{	-3.19658	,	2.58225	,	0	},
{	-3.19752	,	2.57234	,	0	},
{	-3.19855	,	2.56247	,	0	},
{	-3.19969	,	2.55259	,	0	},
};

pos_s straight_path[MAX_STRAIGHT_PATH_LENGTH] = {};
