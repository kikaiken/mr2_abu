/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "constant.h"
#include "mode.h"
#include "user_interface.h"
#include "retarget.h"
#include "lsm6ds3.h"
#include "robotstate.h"
#include "leg.h"
#include "light_math.h"
#include "path.h"
#include "gerge.h"
#include "MadgwickAHRS.h"
#include "adcprocess.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define TUSSOCK_ANGLE_DEG		0.0f
#define SD_TO_TS_ANGLE_DEG		90.0f
#define SANDDUNE_TURN_TIME		500

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
sanddune_status_e sanddune_status;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	wall_sens_s wall_sens;
	wall_pos_s wall_pos;

	//TIM6 Interrupt : CTRL_CYCLE(s)
	if (htim->Instance == TIM6) {
		static int32_t time = 0, gerge_move_time = 0;
		imu_t imu;
		state_s ref_state, state4ctrl;
		static pure_pursuit_status_e pp_status = PF_FOLLOWING;
		field_select_e field_color = GetFieldSW();
		float spd_duty = (GetRotarySwitch(ROTARY_SW_SPEED) + 1) * 0.10f;
		static int8_t starttosd_status = 0, tussock_status = 0, sdtots_status = 0;
		static int32_t sanddune_turn_time;
		static mountain_status_e mountain_status;
		static float mountain_pitch_offset;
		static int32_t mountain_top_time;

		//各種センサデータ取得
		LSM6DS3GetData(&imu);
		MadgwickAHRSupdateIMU(imu.gyro_x, imu.gyro_y, imu.gyro_z, imu.acc_x, imu.acc_y, imu.acc_z);

		//壁センサ
		WallSensorGetDist(&wall_sens);
		GetPosFromWall(&wall_pos, wall_sens);

		LegGetData();
		GetOdometry(imu);

		//経路追従制御
		switch (state.move_mode) {
		//スタートからサンドデューンの経路追従
		case MM_START_TO_SANDDUNE :
			switch (starttosd_status) {
			case 0 :
				//自己位置推定
				if (field_color == FIELD_RED) {integ_pos.x = (state.odom.x * 0.90f) + (  -4.5f - wall_sens.left_front  - (MR2_SENSOR_LFRF / 2))  * 0.10f;}
				else 						  {integ_pos.x = (state.odom.x * 0.90f) + (-(-4.5f - wall_sens.right_front - (MR2_SENSOR_LFRF / 2))) * 0.10f;}
				integ_pos.y = state.odom.y;
				//経路追従
				pp_status = CalcPurePursuit(&ref_state, field_color);
				MotorSetSpdDutyAngvel(spd_duty, ref_state.angvel);
				//経路追従終了
				if (pp_status == PF_FINISH) {
					starttosd_status = 1;
				}
				break;
			case 1 :
				if (field_color == FIELD_RED) {
					if (wall_sens.front_left  > 0.85f) {MotorSetSpdDutyAngvel(spd_duty, (state.angle.yaw) * (-5.00f)); BuzzerOn();}
					else 							   {starttosd_status = 2; BuzzerOff();}
				} else {
					if (wall_sens.front_right > 0.85f) {MotorSetSpdDutyAngvel(spd_duty, (state.angle.yaw) * (-5.00f)); BuzzerOn();}
					else 							   {starttosd_status = 2; BuzzerOff();}
				}
				break;
			case 2 :
				sanddune_status = 0;
				SetMoveMode(MM_SANDDUNE_PREPARE);
				break;
			}
			break;

		//サンドデューンからタソックまで
		case MM_SANDDUNE_TO_TUSSOCK :
			if (field_color == FIELD_RED) {state4ctrl.angle.yaw = state.angle.yaw - DEG2RAD( SD_TO_TS_ANGLE_DEG);}
			else 						  {state4ctrl.angle.yaw = state.angle.yaw - DEG2RAD(-SD_TO_TS_ANGLE_DEG);}
			switch(sdtots_status) {
			case 0 :
				if (field_color == FIELD_RED) {
					if (state.angle.yaw < DEG2RAD(SD_TO_TS_ANGLE_DEG - 20)) {
						MotorSetSpdTurnDuty(0,  0.40);
					} else {
						state.odom.y = 0;
						state.odom.theta = DEG2RAD(270);
						sdtots_status = 1;
					}
				} else {
					if (state.angle.yaw > DEG2RAD(-SD_TO_TS_ANGLE_DEG + 20)) {
						MotorSetSpdTurnDuty(0,  -0.40);
					} else {
						state.odom.y = 0;
						state.odom.theta = DEG2RAD(270);
						sdtots_status = 1;
					}
				}
				break;
			case 1 :
				BuzzerOn();
				if (state.odom.y > -0.70f) {MotorSetSpdDutyAngvel(spd_duty, state4ctrl.angle.yaw * (-4.00f));}
				else 					   {sdtots_status = 2;}
				break;
			case 2 :
				BuzzerOff();
				if (wall_pos.front_wall_dist > 0.80f) {MotorSetSpdDutyAngvel(spd_duty, state4ctrl.angle.yaw * (-4.00f));}
				else 								  {sdtots_status = 3;}
				break;
			default :
				BuzzerOn();
				SetMoveMode(MM_TUSSOCK_PREPARE);
				break;
			}
			break;

		//サンドデューン
		case MM_SANDDUNE :
			if (field_color == FIELD_RED) {state4ctrl.angle.yaw = state.angle.yaw -  (M_PI / 4);}
			else 						  {state4ctrl.angle.yaw = state.angle.yaw - (-M_PI / 4);}
			switch (sanddune_status) {
			case SANDDUNE_BEFORE_TURN :
				if (sanddune_turn_time < SANDDUNE_TURN_TIME) {
					if (field_color == FIELD_RED) {MotorSetSpdTurnDuty(0.00,  0.40);}
					else 						  {MotorSetSpdTurnDuty(0.00, -0.40);}
				} else {
					sanddune_status = SANDDUNE_BEFORE_WALK;
				}
				sanddune_turn_time += CTRL_CYCLE * 1000;
				break;
			case SANDDUNE_BEFORE_WALK :
				if (RAD2DEG(state.angle.pitch) < 10.0f)  {MotorSetSpdDutyAngvel(0.40f, state4ctrl.angle.yaw * (-3.00f)); BuzzerOn();}
				else 									 {sanddune_status = SANDDUNE_TOP;}
				break;
			case SANDDUNE_GO_UP :
				if (RAD2DEG(state.angle.pitch) > 2.0f)	 {MotorSetSpdDutyAngvel(0.42f, state4ctrl.angle.yaw * (-3.00f)); BuzzerOff();}
				else 									 {sanddune_status = SANDDUNE_TOP;}
				break;
			case SANDDUNE_TOP :
				if (RAD2DEG(state.angle.pitch) > -12.0f) {MotorSetSpdDutyAngvel(0.45f, state4ctrl.angle.yaw * (-3.00f)); BuzzerOn();}
				else 									 {sanddune_status = SANDDUNE_GO_DOWN;}
				break;
			case SANDDUNE_GO_DOWN :
				if (RAD2DEG(state.angle.pitch) < -7.0f)  {MotorSetSpdDutyAngvel(0.45f, state4ctrl.angle.yaw * (-3.00f)); BuzzerOff();}
				else 									 {sanddune_status = SANDDUNE_FINISH;}
				break;
			case SANDDUNE_FINISH :
				SetMoveMode(MM_SANDDUNE_TO_TUSSOCK_PREPARE);
				BuzzerOff();
				break;
			default : break;
			}
			break;

		//タソック
		case MM_TUSSOCK :
			if (fabsf(state.angle.yaw - DEG2RAD(-TUSSOCK_ANGLE_DEG)) < DEG2RAD(20) && tussock_status == 0) {
				if (field_color == FIELD_RED) {MotorSetSpdDutyAngvel(0.40f, (state.angle.yaw - DEG2RAD(-TUSSOCK_ANGLE_DEG)) * (-4.30f));}
				else 						  {MotorSetSpdDutyAngvel(0.40f, (state.angle.yaw - DEG2RAD( TUSSOCK_ANGLE_DEG)) * (-4.30f));}
			} else {
				tussock_status = 1;
				if (field_color == FIELD_RED) {
					if (state.angle.yaw < DEG2RAD(-TUSSOCK_ANGLE_DEG)) {MotorSetSpdTurnDuty(0.00f,  0.30f);}
					else											   {MotorSetSpdTurnDuty(0.00f, -0.30f);}
				} else {
					if (state.angle.yaw < DEG2RAD( TUSSOCK_ANGLE_DEG)) {MotorSetSpdTurnDuty(0.00f,  0.30f);}
					else											   {MotorSetSpdTurnDuty(0.00f, -0.30f);}
				}
				if (fabsf(state.angle.yaw - DEG2RAD(-TUSSOCK_ANGLE_DEG)) < DEG2RAD(5)) {tussock_status = 0;}
			}
			break;

		//マウンテン
		case MM_MOUNTAIN :
			switch (mountain_status) {
			case MOUNTAIN_BEFORE_WALK :
				if (RAD2DEG(state.angle.pitch - mountain_pitch_offset) < 8.0f) {MotorSetSpdDutyAngvel(0.38f, (state.angle.yaw * (-4.00f)));}
				else 														   {mountain_status = MOUNTAIN_CLIMBING; BuzzerOn();}
				break;
			case MOUNTAIN_CLIMBING :
				if (RAD2DEG(state.angle.pitch - mountain_pitch_offset) > 4.0f) {MotorSetSpdDutyAngvel(spd_duty, (state.angle.yaw * (-4.00f)));}
				else 														   {mountain_status = MOUNTAIN_TOP; BuzzerOff();}
				break;
			case MOUNTAIN_TOP :
				if (wall_pos.front_wall_dist > 0.75f) {MotorSetSpdDutyAngvel(0.20f, (state.angle.yaw * (-3.00f)));}
				else 								  {uukhai_flag = 1; SetMoveMode(MM_STOP);}
				/*
				if (mountain_top_time < 10) {MotorSetSpdDutyAngvel(spd_duty, (state.angle.yaw * (-4.00f)));}
				else					   {uukhai_flag = 1; SetMoveMode(MM_STOP);}
				mountain_top_time += CTRL_CYCLE * 1000;
				*/
				break;
			default : break;
			}
			break;

		//各モード準備
		case MM_START_TO_SANDDUNE_PREPARE :
			starttosd_status = 0;
			PrepareNextPath(field_color, wall_sens);
			break;
		case MM_SANDDUNE_TO_TUSSOCK_PREPARE :
//			if (field_color == FIELD_RED) {InitMadgwickYaw(DEG2RAD( 45.0f) + wall_pos.rear_wall_angle);}
//			else 						  {InitMadgwickYaw(DEG2RAD(-45.0f) + wall_pos.rear_wall_angle);}
			SetMoveMode(MM_SANDDUNE_TO_TUSSOCK);
			break;
		case MM_SANDDUNE_PREPARE :
			sanddune_turn_time = 0;
			sanddune_status = SANDDUNE_BEFORE_TURN;
			SetMoveMode(MM_SANDDUNE);
			break;
		case MM_TUSSOCK_PREPARE :
			switch (tussock_status) {
			case 0 :
				if (field_color == FIELD_RED) {
					if (state.angle.yaw > DEG2RAD(-TUSSOCK_ANGLE_DEG + 18)) {MotorSetSpdTurnDuty(0.0, -0.40);}
					else 													{tussock_status = 1;}
				} else {
					if (state.angle.yaw < DEG2RAD( TUSSOCK_ANGLE_DEG - 18)) {MotorSetSpdTurnDuty(0.0,  0.40);}
					else 													{tussock_status = 1;}
				}
				break;
			default :
				tussock_status = 0;
				SetMoveMode(MM_TUSSOCK);
				break;
			}
			break;
		case MM_MOUNTAIN_PREPARE :
			mountain_pitch_offset = 0/*state.angle.pitch*/;
			mountain_status = MOUNTAIN_BEFORE_WALK;
			mountain_top_time = 0;
			SetMoveMode(MM_MOUNTAIN);
			break;
		case MM_STOP :
			for (int8_t i = 0; i < 15; i++) {MotorSetSpdTurnDuty(0, 0);}
			break;
		default : break;
		}

		//ゲルゲ制御 : 台形加減速生成はgerge.cの中のGergeTrapezoidMove
		if (state.gerge_move_flag == GERGE_MOVE_READY) {
			gerge_move_time = 0;
			state.gerge_move_flag = GERGE_MOVE_START;
			ref_state.gerge_pos = gerge_pos_map[gerge_move_time];
		} else if (state.gerge_move_flag == GERGE_MOVE_START) {
			gerge_move_time = fminf(gerge_move_time + 1, gerge_trapezoid_end_time);
			ref_state.gerge_pos = gerge_pos_map[gerge_move_time];
		} else if (state.gerge_move_flag == GERGE_MOVE_STOP) {
			ref_state.gerge_pos = state.gerge_pos_temp;
		}
		GergeMotorSetPos(ref_state.gerge_pos);

		//表示等
		if (time > 100) {
//			printf("%f\n", gerge_pos_map[gerge_move_time]);
//			printf("%f %f %f %f %f %f %f %f %f\n", imu.acc_x, imu.acc_y, imu.acc_z, imu.gyro_x, imu.gyro_y, imu.gyro_z, RAD2DEG(estimated_angle.x), RAD2DEG(estimated_angle.y), RAD2DEG(estimated_angle.z));
//			printf("%f %f %f\n", state.odom.x, state.odom.y, RAD2DEG(state.odom.theta));
//			printf("%f %f %f\n", RAD2DEG(state.angle.roll), RAD2DEG(state.angle.pitch), RAD2DEG(state.angle.yaw));
//			printf("%f %f\n", wall_sens.left_front, wall_sens.right_front);
//			printf("%f %f\n", wall_pos.rear_wall_dist, wall_pos.rear_wall_angle);
//			printf("%f\n", (state.odom.theta * (-0.05f)) + ((wall_sens.left_front - 0.35f) * 6.0f));
//			printf("%f %f %f\n", RAD2DEG(estimated_angle.x), RAD2DEG(estimated_angle.y), RAD2DEG(estimated_angle.z));
//			printf("%f %f\n", integ_pos.x, integ_pos.y);
			time = 0;
		} else {
			time += CTRL_CYCLE * 1000;
		}
	}

	//TIM7 Interrupt : CTRL_CYCLE(s)
	if (htim->Instance == TIM7) {
		//ゲルゲ機構
		GetGergePos();
	}
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  volatile int8_t mode;
  volatile int8_t init_imu_status, init_enc_status;

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_SPI1_Init();
  MX_SPI3_Init();
  MX_TIM3_Init();
  MX_TIM8_Init();
  MX_USART1_UART_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  /* USER CODE BEGIN 2 */
  RetargetInit();	//setting stdio(printf / scanf)
  printf("\n\nMR2 : Initializing...\n");

  init_imu_status = LSM6DS3Init();	//IMU:LSM6DS3
  init_enc_status = AMT203Init();	//AMT203

  //エラー判定
  if (init_enc_status == MY_ERROR_NONE && init_imu_status == IMU_SUCCESS) {
	  printf("MR2 : Initialize Succeeded\n");
  } else {
	  printf("MR2 : Initialize Failed\n");
	  UIWarning();
  }

  UIStartUp();

  //ジャイロのオフセット取得
  if (init_imu_status == IMU_SUCCESS) {		//通信に成功していたら
	  LSM6DS3GetOffset();
  }

  //モータRESETピンHIGH
  MotorEnable();
  GergeMotorEnable();

  ADCEnable();

  //TIM7起動
  HAL_TIM_Base_Start_IT(&htim7);
  HAL_Delay(100);

  //ゲルゲ機構リセット
  GergeGetZeroPos();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	//MODEロータリースイッチでモードを選択して、左のボタンを押すとmode.cの各関数を実行
	if (ButtonIsOn(2)) {
		ButtonJudgePushTime(2, 0);
		mode = GetRotarySwitch(ROTARY_SW_MODE);
		ExecMode(mode);
	}
	//フィールド色表示
	if (GetFieldSW() == FIELD_RED) {UILEDSet(0b0000); IndicateField(FIELD_RED);}
	else						   {UILEDSet(0b0001); IndicateField(FIELD_BLUE);}
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 144;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
