/*
 * retarget.c
 *
 *  Created on: 2019/01/02
 *      Author: Ryohei
 */

#include "stm32f4xx_hal.h"
#include "retarget.h"
#include <stdio.h>
#include "common.h"
#include "uart_terminal.h"

void RetargetInit(void) {
	UartTerminalInit(&UART_HANDLER);
	setbuf(stdin, NULL);
	setbuf(stdout, NULL);
	setbuf(stderr, NULL);
}


#ifdef __GNUC__
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

PUTCHAR_PROTOTYPE {
	UartTerminalSend(ch);
	return 1;
}

#ifdef __GNUC__
#define GETCHAR_PROTOTYPE int __io_getchar(void)
#else
#define GETCHAR_PROTOTYPE int fgetc(FILE *f)
#endif /* __GNUC__ */

GETCHAR_PROTOTYPE {
	return UartTerminalRecv();
}
