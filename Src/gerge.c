/*
 * gerge.c
 *
 *  Created on: 2018/12/23
 *      Author: Ryohei
 */

#include "gerge.h"
#include "user_interface.h"
#include "adcprocess.h"
#include <math.h>
#include <stdio.h>

//#define DEBUG
#define GERGE_POS_PGAIN		(-4.50f)
#define GERGE_POS_DGAIN		(-11.00f)

//グローバル変数
int16_t gerge_trapezoid_end_time = 0;
float gerge_vel_map[GERGE_VEL_MAP_LONG];
float gerge_pos_map[GERGE_POS_MAP_LONG];

/*
 * ゲルゲキャッチからのスタート
 * @param
 * @return
 */
void GergeCatchStart(void) {
	GergeHandOpen();
	while (GergeIsToCatch() == 0);		//ハンドマイクロスイッチが反応するまで待ち
	GergeHandClose();

	//MR1が離すまで待つ
	HAL_Delay(500);
}

/*
 * ゲルゲキャッチ
 * @param
 * @return
 */
void GergeCatchAndWaitButton(void) {
	GergeHandOpen();
	while (GergeIsToCatch() == 0);		//ハンドマイクロスイッチが反応するまで待ち
	GergeHandClose();

	//ボタン待ち
	while (ButtonIsOn(2) == BUTTON_OFF);
	ButtonJudgePushTime(2, 0);
}

/*
 * ゲルゲ持ち上げてウーハイ
 * @param
 * @return
 */
void GergeUukhai(void) {
	GergeTrapezoidMove(state.gerge_pos, 0.34, 2.0, 5.0);
}

/*
 * ゲルゲを一番下まで下げる
 * @param
 * @return
 */
void GergeReturnToBottom(void) {
	GergeTrapezoidMove(state.gerge_pos, 0.0, 2.0, 2.0);
}

/*
 * ゲルゲ機構を台形加減速
 * @param	start	: 初期位置
 * 			goal	: ゴール位置
 * 			vel_max : 最大速度
 * 			acc_max : 加速度/減速度
 * @return
 */
void GergeTrapezoidMove(float start, float goal, float vel_max, float acc_max) {
	int16_t path_long_mm = fminf(fabsf(start - goal) / GERGE_VEL_MAP_INTERVAL, GERGE_VEL_MAP_LONG - 1);
    int16_t time_10ms = 0;
    float imaginary_pos;

    //ゲルゲ生成中は動かさない
    state.gerge_pos_temp = state.gerge_pos;
    state.gerge_move_flag = GERGE_MOVE_STOP;

	//制限速度を設定
	gerge_vel_map[0] = 0;
	for (int16_t i = 1; i < path_long_mm; i++) {
		gerge_vel_map[i] = vel_max;
	}
	for (int16_t i = path_long_mm; i < GERGE_VEL_MAP_LONG; i++) {
		gerge_vel_map[i] = 0;
	}

	//加速
    for (int16_t i = 0; i < path_long_mm; i++) {
    	gerge_vel_map[i + 1] = fminf(gerge_vel_map[i + 1], sqrtf(powf(gerge_vel_map[i], 2) + 2 * acc_max * GERGE_VEL_MAP_INTERVAL));
    }
    //減速
    for (int16_t i = path_long_mm - 1; i >= 0; i--) {
        gerge_vel_map[i] = fminf(gerge_vel_map[i], sqrtf(powf(gerge_vel_map[i + 1], 2) + 2 * acc_max * GERGE_VEL_MAP_INTERVAL));
    }

    //時間に対する位置のマップに変換
    //位置マップクリア
    for (int16_t i = 0; i < GERGE_POS_MAP_LONG; i++) {
    	gerge_pos_map[i] = 0;
    }
    gerge_pos_map[0] = imaginary_pos = start;
    if (start < goal) {		//上がるとき
    	while (gerge_vel_map[(int)(fabsf(imaginary_pos - start) / GERGE_VEL_MAP_INTERVAL) + 1] != 0) {
    		time_10ms++;
    		if (fabsf(imaginary_pos - start) < GERGE_VEL_MAP_INTERVAL) {
    			imaginary_pos += gerge_vel_map[(int)(fabsf(imaginary_pos - start) / GERGE_VEL_MAP_INTERVAL) + 1] * GERGE_POS_MAP_INTERVAL;
    		} else {
    			imaginary_pos += gerge_vel_map[(int)(fabsf(imaginary_pos - start) / GERGE_VEL_MAP_INTERVAL)] * GERGE_POS_MAP_INTERVAL;
    		}
    		gerge_pos_map[time_10ms] = imaginary_pos;
    	}
    } else {				//下がるとき
    	while (gerge_vel_map[(int)(fabsf(imaginary_pos - start) / GERGE_VEL_MAP_INTERVAL) + 1] != 0) {
    		time_10ms++;
    		if (fabsf(imaginary_pos - start) < GERGE_VEL_MAP_INTERVAL) {
    			imaginary_pos -= gerge_vel_map[(int)(fabsf(imaginary_pos - start) / GERGE_VEL_MAP_INTERVAL) + 1] * GERGE_POS_MAP_INTERVAL;
    		} else {
    			imaginary_pos -= gerge_vel_map[(int)(fabsf(imaginary_pos - start) / GERGE_VEL_MAP_INTERVAL)] * GERGE_POS_MAP_INTERVAL;
    		}
    		gerge_pos_map[time_10ms] = imaginary_pos;
    	}
    }

    //表示
#ifdef DEBUG
    for (int16_t i = 0; i <= time_10ms; i++) {
    	printf("%f\n", gerge_pos_map[i]);
    	HAL_Delay(1);
    }
#endif

    //終了時間をグローバル変数に保存
    gerge_trapezoid_end_time = time_10ms;

    //動作開始
    state.gerge_move_flag = GERGE_MOVE_READY;
}

/*
 * レール位置を一番下まで下げてゼロとする
 * @param
 * @return
 */
void GergeGetZeroPos(void) {
	while (GergeLimitIsOn() == 0) {
		GergeMotorSetDuty(-0.07);
	}
	GergeMotorSetDuty(0);
	state.gerge_pos_offset = state.gerge_pos;
}

/*
 * ゲルゲモータ位置制御
 * @param	ref_pos : 目標位置
 * @return
 */
void GergeMotorSetPos(float ref_pos) {
	if (ref_pos >= GERGE_POS_BOTTOM && ref_pos <= GERGE_POS_TOP) {			//範囲外に出たら制御しない
		GergeMotorSetDuty((state.gerge_pos - ref_pos) * GERGE_POS_PGAIN + (state.gerge_pos - state.prev_gerge_pos) * GERGE_POS_DGAIN + 0.05);
	}
}

/*
 * レールモータを指定したDutyで回す
 * @param	duty : モータに与えるDuty(-1 ~ 1)
 * @return
 */
void GergeMotorSetDuty(float duty) {
	//回転方向を指定
	if (duty >= 0) {
		HAL_GPIO_WritePin(GERGE_MOTOR_DIR_1_PORT, GERGE_MOTOR_DIR_1_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GERGE_MOTOR_DIR_2_PORT, GERGE_MOTOR_DIR_2_PIN, GPIO_PIN_SET);
	} else {
		HAL_GPIO_WritePin(GERGE_MOTOR_DIR_1_PORT, GERGE_MOTOR_DIR_1_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GERGE_MOTOR_DIR_2_PORT, GERGE_MOTOR_DIR_2_PIN, GPIO_PIN_RESET);
	}
	//Duty制限をかけ絶対値をとる
	 duty = fabsf(fmaxf(fminf(duty, GERGE_DUTY_LIMIT), -GERGE_DUTY_LIMIT)) * (float)(GERGE_TIM_FREQ / GERGE_PWM_FREQ);

	//チャンネルにコンペアマッチ値を設定
	__HAL_TIM_SetCompare(&GERGE_MOTOR_TIM_HANDLER, GERGE_MOTOR_TIM_CH, duty);
}

/*
 * レールモータENABLE
 * @param
 * @return
 */
void GergeMotorEnable(void) {
	GergeMotorSetDuty(0);
	HAL_TIM_PWM_Start(&GERGE_MOTOR_TIM_HANDLER, GERGE_MOTOR_TIM_CH);
}

/*
 * レールモータDISABLE
 * @param
 * @return
 */
void GergeMotorDisable(void) {
	GergeMotorSetDuty(0);
	HAL_TIM_PWM_Stop(&GERGE_MOTOR_TIM_HANDLER, GERGE_MOTOR_TIM_CH);
}

/*
 * ゲルゲのリミットスイッチが反応しているか
 * @param
 * @return	押されている:1/押されていない:0
 */
int8_t GergeLimitIsOn(void) {
	if (HAL_GPIO_ReadPin(GERGE_LIMIT_SW_PORT, GERGE_LIMIT_SW_PIN) == GPIO_PIN_RESET) {return 1;}
	else                                                                           	 {return 0;}
}

/*
 * ゲルゲハンドを閉じる
 * @param
 * @return
 */
void GergeHandClose(void) {
	HAL_GPIO_WritePin(GERGE_HAND_CYLINDER_PORT, GERGE_HAND_CYLINDER_PIN, GPIO_PIN_SET);
}

/*
 * ゲルゲハンドを開く
 * @param
 * @return
 */
void GergeHandOpen(void) {
	HAL_GPIO_WritePin(GERGE_HAND_CYLINDER_PORT, GERGE_HAND_CYLINDER_PIN, GPIO_PIN_RESET);
}

/*
 * ゲルゲが掴める状態かどうか判定する
 * @param
 * @return	掴める:1/掴めない:0
 */
int8_t GergeIsToCatch(void) {
	if (HAL_GPIO_ReadPin(GERGE_TOUCH_SW_PORT, GERGE_TOUCH_SW_PIN) == GPIO_PIN_RESET) {return 1;}
	else                                                                           	 {return 0;}
}
