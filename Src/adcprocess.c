/*
 * wall_sensor.c
 *
 *  Created on: 2019/03/03
 *      Author: Ryohei
 */

#include "adcprocess.h"
#include "user_interface.h"
#include "constant.h"
#include "mode.h"
#include <math.h>

//定数
#define ADC_DATA_BUFFER_SIZE	((uint32_t) 8)
#define MOVAVE_NUM				10
#define GERGE_POS_MOVAVE_NUM	10

#define CONTACTLESS_SIG_THRES	0.30f					//非接触の合図のしきい値(m) : これより近くを検出したら合図

//ファイル内変数
static uint16_t adc_data[ADC_DATA_BUFFER_SIZE];			//センサ生値
static float wall_sens_movave[ADC_DATA_BUFFER_SIZE];	//センサ移動平均値
static float gerge_pos_movave;

/*
 * 左右壁に対する横ずれ・前後壁に対する距離と角度を求める
 * @param
 * @return
 */
void GetPosFromWall(wall_pos_s *pos, wall_sens_s sens) {
	const float center_expect_long = (FIELD_WIDTH_UR1_TO_SAND - MR2_SENSOR_LFRF) / 2;	//中央にいた場合の左右壁までの距離
	//横ずれ
	pos->side_wall_error =  ((sens.left_front - center_expect_long) - (sens.right_front - center_expect_long)) / 2;	//左右センサから求めた距離の平均値をとる

	//前壁
	pos->front_wall_dist  = (sens.front_left + sens.front_right) / 2;
	pos->front_wall_angle = atan2f(sens.front_left - sens.front_right, MR2_SENSOR_FLFR);

	//後壁
	pos->rear_wall_dist   = (sens.rear_left + sens.rear_right) / 2;
	pos->rear_wall_angle  = atan2f(sens.rear_right - sens.rear_left, MR2_SENSOR_RLRR);
}

/*
 * 各壁センサのAD変換値を距離に変換して収納
 * @param
 * @return
 */
void WallSensorGetDist(wall_sens_s *wall_sens) {
	static int16_t call_counter = 0;
	static int16_t wall_sens_buff[ADC_DATA_BUFFER_SIZE][MOVAVE_NUM] = {};
	static int8_t movave_index = 0;

	//移動平均
	for (int8_t i = 0; i < ADC_DATA_BUFFER_SIZE; i++) {
		wall_sens_buff[i][movave_index] = adc_data[i];
		wall_sens_movave [i] = 0;
	}
	for (int8_t i = 0; i < ADC_DATA_BUFFER_SIZE; i++) {
		for (int8_t j = 0; j < MOVAVE_NUM; j++) {
			wall_sens_movave[i] += wall_sens_buff[i][j];
		}
		wall_sens_movave[i] /= MOVAVE_NUM;
	}
	movave_index = (movave_index + 1) % MOVAVE_NUM;

	if (call_counter < MOVAVE_NUM) {	//呼び出し回数が少ないときは生値を使う
		wall_sens->front_left  = 0.57708078f * powf(((float)adc_data[0] * 3.3) / (float)4096, -1.07014571f);
		wall_sens->front_right = 0.57708078f * powf(((float)adc_data[1] * 3.3) / (float)4096, -1.07014571f);
		wall_sens->left_front  = 0.57708078f * powf(((float)adc_data[2] * 3.3) / (float)4096, -1.07014571f);
		wall_sens->right_front = 0.57708078f * powf(((float)adc_data[4] * 3.3) / (float)4096, -1.07014571f);
		wall_sens->rear_left   = 0.57708078f * powf(((float)adc_data[6] * 3.3) / (float)4096, -1.07014571f);
		wall_sens->rear_right  = 0.57708078f * powf(((float)adc_data[7] * 3.3) / (float)4096, -1.07014571f);
		call_counter++;
	} else {	//バッファが溜まったら移動平均値を使う
		//移動平均AD変換値を距離に変換
		wall_sens->front_left  = 0.57708078f * powf(((float)wall_sens_movave[0] * 3.3) / (float)4096, -1.07014571f);
		wall_sens->front_right = 0.57708078f * powf(((float)wall_sens_movave[1] * 3.3) / (float)4096, -1.07014571f);
		wall_sens->left_front  = 0.57708078f * powf(((float)wall_sens_movave[2] * 3.3) / (float)4096, -1.07014571f);
		wall_sens->right_front = 0.57708078f * powf(((float)wall_sens_movave[4] * 3.3) / (float)4096, -1.07014571f);
		wall_sens->rear_left   = 0.57708078f * powf(((float)wall_sens_movave[6] * 3.3) / (float)4096, -1.07014571f);
		wall_sens->rear_right  = 0.57708078f * powf(((float)wall_sens_movave[7] * 3.3) / (float)4096, -1.07014571f);
	}


	//非接触の合図用
	if (wall_sens->left_front < CONTACTLESS_SIG_THRES || wall_sens->right_front < CONTACTLESS_SIG_THRES) {contactless_sig = 1;}
	else																							     {contactless_sig = 0;}
}

/*
 * ゲルゲ機構位置取得
 * @param
 * @return
 */
void GetGergePos(void) {
	static int16_t pos_buff[GERGE_POS_MOVAVE_NUM] = {};
	static int8_t movave_index = 0;

	//移動平均
	pos_buff[movave_index] = adc_data[3];
	for (int8_t i = 0; i < GERGE_POS_MOVAVE_NUM; i++) {
		gerge_pos_movave += pos_buff[i];
	}
	gerge_pos_movave /= GERGE_POS_MOVAVE_NUM;
	movave_index = (movave_index + 1) % GERGE_POS_MOVAVE_NUM;

	state.prev_gerge_pos = state.gerge_pos;		//前回値保存
	state.gerge_pos = fmaxf((GERGE_POT_GEAR_DIAMETER * M_PI * 10.0f * (float)gerge_pos_movave * 3.3f) / (4096.0f * 2.5f) - state.gerge_pos_offset, 0);	//今回値計算
}

/*
 * AD変換・DMA転送開始
 * @param
 * @return
 */
void ADCEnable(void) {
	HAL_ADC_Start_DMA(&ADC_HANDLER, (uint32_t *)adc_data, ADC_DATA_BUFFER_SIZE);
}

/*
 * AD変換・DMA転送停止
 * @param
 * @return
 */
void ADCDisable(void) {
	HAL_ADC_Stop_DMA(&ADC_HANDLER);
}
